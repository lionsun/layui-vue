export type DropdownTrigger = "click" | "hover" | "contextMenu";
